import sys
import json

if len(sys.argv) < 3 :
    print(f'Usage python {sys.argv[0]} magyc_products_config_file key value')

config_file = sys.argv[1]
key = sys.argv[2]
new_destination = sys.argv[3]

obj = {}
with open(config_file,"r") as file:
    obj = json.load(file)

with open(config_file,"w") as file:
    obj[key] = new_destination
    json.dump(obj, file, indent=4);