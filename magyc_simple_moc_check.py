import sys
from typing import List
from mock.BaseJob import BaseJob

class Job(BaseJob):

    def __init__(self):
        super().__init__('simple_mock_check')

    def build_parameters(self, args: List[str]):
        self.parameters = {
            "ra": args[1],
            "dec": args[2],
            "radius": '10.0'
        }

    def build_output(self, args: List[str]):
        self.output_dir_name = args[5].split('=')[1]
    

job = Job()
job.run(sys.argv)
