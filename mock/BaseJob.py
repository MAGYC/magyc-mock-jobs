from abc import ABC, abstractmethod
from pathlib import Path
import random
import sys
import time
from typing import Dict, List

from magyc.common import build_key_from_parameters, find_config, resolve_keys


class BaseJob(ABC):
    
    def __init__(self, name: str) -> None:
        self.name = name
        self.config = find_config()
        assert 'directory' in self.config
        self.output_dir_name = "./nowhere"
        self.parameters: Dict[str, str] = {}

    def run(self, args: List[str]):
        print(args)
        try:
            self.parse_args(args)
        except IndexError as ie:
            print(f"ERROR {ie}. ARGV is {sys.argv}",file=sys.stderr)
            raise ie
        self.do_run()


    def parse_args(self, args: List[str]):
        self.build_parameters(args)
        self.build_output(args)
        print(f'PARAMS={self.parameters}, OUTPUT={self.output_dir_name}') 

    @abstractmethod
    def build_parameters(self, args: List[str]):
        ...

    @abstractmethod
    def build_output(self, args: List[str]):
        ...

    @staticmethod
    def copytree(src, dst, symlinks=False, ignore=None):
        import os, shutil
        for item in os.listdir(src):
            s = os.path.join(src, item)
            d = os.path.join(dst, item)
            if os.path.isdir(s):
                shutil.copytree(s, d, symlinks, ignore)
            else:
                shutil.copy2(s, d)

    def get_name(self):
        return self.name

    def do_run(self):
        names = resolve_keys(self.get_name(), key_config=self.config)
        key = build_key_from_parameters(self.get_name(), self.parameters, names)
        destination = Path(self.config['directory'] / Path(key))
        if Path.exists(destination):
            output_dir_path = Path(self.output_dir_name)
            print(f'Copy data from {destination} to {output_dir_path}')
            BaseJob.copytree(destination, output_dir_path)
        else : 
            print(f"No such directory {destination}. Please download data first with magycli.",file=sys.stderr)
        time.sleep(random.randrange(20))
