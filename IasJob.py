import sys
from typing import List
from mock.BaseJob import BaseJob

class IasJob(BaseJob):
    def build_parameters(self, args: List[str]):
        self.parameters = {
            "ra": args[1],
            "dec": args[2],
            "radius": args[3]
        }

    def build_output(self, args: List[str]):
        self.output_dir_name = args[5].split('=')[1]


